FROM node:10
WORKDIR /test
COPY package.json /test
RUN npm install
COPY . /test
CMD node node.js

FROM nginx
WORKDIR /etc/nginx/conf.d
COPY nginx.conf /etc/nginx/conf.d
